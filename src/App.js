// Base Imports
import React, { useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';



// App Components
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import NotFound from './pages/NotFound';

//App Imports
//UserContext from React.createContext
import UserContext from './UserContext'

const App = () => {
	//useState funcction
	const [user, setUser] =useState(null)
	
	return (
	//UserContext <- object and provider is a function to store a global properties	
	<UserContext.Provider value = {{user, setUser}}>
		<BrowserRouter>
			<AppNavBar/>
			<Routes>
				<Route path ="/" element = {<Home />} />
				<Route path ="/courses" element = {<Courses />} />
				<Route path ="/register" element = {<Register />} />
				<Route path ="/login" element = {<Login />} />
				<Route path = "/logins" element = {<NotFound />} />
			</Routes>
		</BrowserRouter>
	</UserContext.Provider>
	)
}

export default App;