import	{ useState, useEffect, useContext } from 'react';

//Bootstrap Components

import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

// App import
import UserContext from '../UserContext';

const Login = () => {
	
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState('');

	//deconstruct
    const {user, setUser} = useContext(UserContext)


	useEffect( () => {
		let isEmailisNotEmpty = email !== '';
		let isPasswordNotEmpty = password !== '';

		//Determine if all condition are met

		if(isEmailisNotEmpty && isPasswordNotEmpty){
			setIsDisabled(false)
		}else{
			setIsDisabled(true)
		}

	}, [email,password])

	const login = (e) => {
		e.preventDefault()
		alert('You are now logged in')
        
		setUser({email: email})

		setEmail('');
		setPassword('');
	}

	return (
		<Container fluid>
			<h3>Login</h3>
			<Form onSubmit = {login}>
				<Form.Group>
					<Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" required value ={email} onChange = { (e) => setEmail(e.target.value)}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" required value = {password} onChange = { (e) => setPassword(e.target.value)}/>
				</Form.Group>
				<Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
			</Form>
		</Container>
	);
}

export default Login;