

//Page and Components
// import Home from './pages/Home'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
//import Counter from './components/Counter'
import App from './App'

//Base Imports
import React from 'react';
import ReactDOM from 'react-dom';
import{ BrowserRouter, Routes, Route} from 'react-router-dom'
import AppNavBar from './components/AppNavBar';

//CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css'

//Bootstrap Components


ReactDOM.render(
  <React.StrictMode>
    {/* <AppNavBar /> */}
    {/* <Courses /> */}
    {/* <Counter /> */}
    {/* <Register /> */}
    {/* <Login /> */}
    <App/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

