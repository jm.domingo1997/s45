// Bootstrap Components
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { useState, useEffect } from 'react';

export default function Course(props) {
	let course = props.course;
	//let word = props.word;

    const [ isDisabled, setIsDisabled ] = useState(false)
    const [seats, setSeats ] = useState(10)

    useEffect( () => {
        if(seats === 0){
            setIsDisabled(true)
        }
    }, [seats])

    /* const [count, setCount] = useState(0)
    const [seats, setSeats] = useState(30)

    const enroll = () => {
        if(seats == 0){
            alert('Course is Full!')
        }else{
            setCount(count + 1);
            setSeats(seats - 1);
        }

    } */

	return(
		<Card>
            <Card.Body>
                <Card.Title>{course.name} {/* {word} */}</Card.Title>
                <h6>Description:</h6>
                <p>{course.description}</p>
                 <h6>Price:</h6>
                 <p>{course.price}</p>
                 <h6>Seats</h6>
                 <p>{seats} remaining</p>
                 {/* <h6>Enrollees</h6> */}
                 {/* <p>{count}</p> */}
                <Button variant="primary" onClick ={ () => setSeats(seats-1) } disabled = {isDisabled}>Enroll</Button>
            </Card.Body>
        </Card>
		)
}

